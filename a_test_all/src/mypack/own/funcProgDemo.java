package mypack.own;

@FunctionalInterface
interface example{
	public void hi();
}

@FunctionalInterface
interface addition{
	public int add(int a, int b);
}

public class funcProgDemo {

	public static void main(String[] args) {
		example e = ()-> System.out.println("hello..hi..");
		e.hi();
		
		addition a = (x,y)->x+y;
		System.out.println(a.add(10, 20));
	}
}
